const Epub = require("epub-gen");
const fs = require('fs');
const path = require("path");


const inputFolder = 'output/You-Dont-Know-JS-2nd-ed/'

function readDirSync(inFolder, option) {
    option.output =  `epub/${path.basename(inFolder)}.epub`
    option.tile = `${path.basename(inFolder)}`;
    const files = fs.readdirSync(inFolder) ;
    for (const file of files) {
        const fullFilePath = path.resolve(inFolder + '/' + file);
        console.log('Processing ' + fullFilePath)
        const isDir = fs.lstatSync(fullFilePath).isDirectory();
        if (!isDir) {
            let data = fs.readFileSync(fullFilePath).toString();
            const folderPath = path.resolve(inFolder)
            data = data.replace(/ src="images/g, ` src="${folderPath}/images`)
            data = data.replace(/ src='images/g, ` src='${folderPath}/images`)
            option.content.push({ title:file, data , beforeToc: true})
        }
    }
    option.content.sort(sortContent);
}



fs.readdir(inputFolder, (err, files) => {
    files.forEach(file => {
        const fullFilePath = path.resolve(inputFolder + '/' + file);
        console.log('Processing ' + fullFilePath)
        const isDir = fs.lstatSync(fullFilePath).isDirectory();
        if (isDir) {
            const option = {
                author: "Kyle Simpson",
                title: 'You-Dont-Know-JS-2nd-ed',
                content: [],
                appendChapterTitles : false
            }
            readDirSync(fullFilePath,option)
            new Epub(option).promise.then(
                () => console.log("Ebook Generated Successfully!"),
                err => console.error("Failed to generate Ebook because of ", err)
                );
        }
    })
});

function sortContent(x,y){
    const a = x.title;
    const b = y.title;
    const aFirst2Chars = a.substring(0,2)
    const bFirst2Chars = b.substring(0,2) ;
    
  if (aFirst2Chars === bFirst2Chars) {
    return normalSort(a,b);
  }
  if (aFirst2Chars === "ch" && bFirst2Chars === 'ap') {
    return -1  
  }
  if (aFirst2Chars === "ap" && bFirst2Chars === 'ch') {
    return 1  
  }
  if (aFirst2Chars === "to" && bFirst2Chars === 'fo') {
    return -1  
  }
  if (aFirst2Chars === "fo" && bFirst2Chars === 'to') {
    return 1  
  }
  if (aFirst2Chars === 'ap') {
    return 1  
  }
  if ( bFirst2Chars === 'ap') {
    return -1  
  }
  if (aFirst2Chars === 'ch') {
    return 1  
  }
  if ( bFirst2Chars === 'ch') {
    return -1  
  }

  return normalSort(a,b);
}

function normalSort(x,y) {
    if (x < y) {
        return -1;
      }
      if (x > y) {
        return 1;
      }
      return 0;
}