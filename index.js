"use strict"

const showdown  = require('showdown');
const converter = new showdown.Converter();
const path = require("path");

const fs = require('fs')
const inputFolder = "input/"
const outputFolder = "output/";


function readfiles(inFolder, outFolder, fileCallback, dirCallback) {
    fs.readdir(inFolder, function(err, files){
        err && console.log(err);
        files.forEach(file => {
            const fullInPath = path.resolve(inFolder + '/' + file);
            console.log('Processing ' + fullInPath)
            const isDir = fs.lstatSync(fullInPath).isDirectory();
            if (isDir) {
                dirCallback.call(this, outFolder, file, fullInPath);
            } else {
                fileCallback.call(this, outFolder, file, fullInPath);
            }
        });
    });

}
function dirCallback(outFolder, file , fullInPath) {
    const fullOutPath = `${outFolder}/${file}`;
    const newFileFullPath = path.resolve(fullOutPath);
    !fs.existsSync(newFileFullPath) && fs.mkdirSync(newFileFullPath);
    readfiles(fullInPath, fullOutPath, fileCallback, dirCallback);
}

function fileCallback(outFolder, file, fullInPath) {
    const fullOutPath = `${outFolder}/${file}`;
    const newFileFullPath = path.resolve(fullOutPath);
    if (!fullInPath.endsWith('.md')) {
        fs.copyFile(fullInPath, newFileFullPath, ()=>{});
        return;
    }
    const outFilePath = newFileFullPath.replace(new RegExp('.md$'), '.html');
    !fs.existsSync(outFilePath) && fs.readFile(fullInPath, {}, (err, data) => {
            err && console.log(err)
            let html = converter.makeHtml(data.toString());

            const mdHrefSingleQuoteMark = /href='[^']+\.md'/g
            html = updateHtmlLinks(html, mdHrefSingleQuoteMark, "href='http", /.md'$/,  ".html'")

            const mdHrefDoubleQuoteMark = /href="[^"]+\.md"/g
            html = updateHtmlLinks(html, mdHrefDoubleQuoteMark, 'href="http', /.md"$/,  '.html"')


            fs.writeFileSync(outFilePath, html);
    });
}

function updateHtmlLinks(html, hrefRegex, ingoreHrefThatStartsWith, extensionToBeReplacedRegex, newExtenstion) {
    const matches = html.match(hrefRegex) || [];
    matches.filter(e=> ! e.startsWith(ingoreHrefThatStartsWith));               
    for (const m of matches) {
        const newHref = m.replace(extensionToBeReplacedRegex, newExtenstion);
        html = html.replace(m, newHref);
    }
    return html;
}


readfiles(inputFolder, outputFolder, fileCallback, dirCallback);
